import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Task} from "../models/task";
import {Observable} from "rxjs";

const API_URL = 'http://localhost:8080/api/tasks'

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  constructor(private http: HttpClient) {
  }

  getTasks(): Observable<Task[]> {
    return this.http.get<Task[]>(API_URL)
  }

  addTask(task: Task): Observable<Task> {
    return this.http.post<Task>(API_URL, task)
      .pipe(
        //  handle error
      );
  }
}
