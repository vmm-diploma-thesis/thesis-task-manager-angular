export class Task {
  constructor(description: string, dateDone: string, hoursWorked: number, id?: number) {
    this.id = id ?? 0;
    this.description = description;
    this.dateDone = dateDone;
    this.hoursWorked = hoursWorked;
  }

  id: number;
  description: string;
  dateDone: string;
  hoursWorked: number;
}
