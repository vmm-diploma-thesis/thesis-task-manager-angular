import {Component, OnInit} from '@angular/core';
import {TaskService} from "../../services/task.service";
import {Task} from "../../models/task";
import { Title } from '@angular/platform-browser';


@Component({
  selector: 'app-task-overview',
  templateUrl: './task-overview.component.html',
  styleUrls: ['./task-overview.component.scss']
})
export class TaskOverviewComponent implements OnInit {

  displayedColumns: string[] = ['id', 'description', 'dateDone', 'hoursWorked'];
  tasks: Task[] = [];

  constructor(private taskService: TaskService,
              private titleService: Title) {
  }

  ngOnInit(): void {
    this.titleService.setTitle('Task Overview')
    this.taskService
      .getTasks()
      .subscribe({
        next: data => this.tasks = data,
        error: err => this.handleError(err)
      })
  }

  private handleError(error: any) {
    //  TODO
  }
}
