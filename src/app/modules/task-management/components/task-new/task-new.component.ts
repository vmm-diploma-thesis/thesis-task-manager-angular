import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { Title } from '@angular/platform-browser';
import {Task} from "../../models/task";
import { TaskService } from '../../services/task.service';
import {Router} from "@angular/router";


@Component({
  selector: 'app-task-new',
  templateUrl: './task-new.component.html',
  styleUrls: ['./task-new.component.scss']
})
export class TaskNewComponent implements OnInit {
  taskForm: FormGroup = this.formBuilder.group({
    description: ['', Validators.required],
    dateDone: ['',  Validators.required],
    hoursWorked: ['', Validators.required]
  })

  constructor(private formBuilder: FormBuilder,
              private taskService: TaskService,
              private titleService: Title,
              private router: Router) {
  }

  ngOnInit(): void {
    this.titleService.setTitle('Task New')
  }

  onSubmit() {
    let value = this.taskForm.value
    let task = new Task(
      value.description,
      value.dateDone,
      value.hoursWorked
    )

    this.taskService
      .addTask(task)
      .subscribe(data => {
        // data logic
        this.router.navigate([''])
      })
  }
}
