import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TaskOverviewComponent} from './components/task-overview/task-overview.component';
import {TaskNewComponent} from './components/task-new/task-new.component';
import {MatTableModule} from "@angular/material/table";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {ReactiveFormsModule} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";
import {RouterModule} from "@angular/router";


@NgModule({
  declarations: [
    TaskOverviewComponent,
    TaskNewComponent
  ],
  imports: [
    CommonModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatButtonModule,
    RouterModule
  ]
})
export class TaskManagementModule {
}
