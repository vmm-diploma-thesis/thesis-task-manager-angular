import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TaskOverviewComponent} from "./modules/task-management/components/task-overview/task-overview.component";
import {TaskNewComponent} from "./modules/task-management/components/task-new/task-new.component";

const routes: Routes = [
  {path: '', component: TaskOverviewComponent},
  {path: 'new', component: TaskNewComponent},
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
